﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;
public class PersonageController : MonoBehaviour
{
    public GameObject countDown;
    public GameObject SpineBase;
    public GameObject SpineMid;
    public GameObject Neck;
    public GameObject Head;
    public GameObject ShoulderLeft;
    public GameObject ElbowLeft;
    public GameObject WristLeft;
    public GameObject HandLeft;
    public GameObject ShoulderRight;
    public GameObject ElbowRight;
    public GameObject WristRight;
    public GameObject HandRight;
    public GameObject HipLeft;
    public GameObject KneeLeft;
    public GameObject AnkleLeft;
    public GameObject FootLeft;
    public GameObject HipRight;
    public GameObject KneeRight;
    public GameObject AnkleRight;
    public GameObject FootRight;
    public GameObject SpineShoulder;
    public GameObject HandTipLeft;
    public GameObject ThumbLeft;
    public GameObject HandTipRight;
    public GameObject ThumbRight;
    private Dictionary<JointType, GameObject> _BoneMap;
    private BodySourceManager bodySourceManager;
    public Vector3[] additiveRotations;
  

    public void Start()
    {
        bodySourceManager = GameObject.FindObjectOfType<BodySourceManager>();
       
        _BoneMap = new Dictionary<JointType, GameObject>()
        {
            { JointType.SpineBase, SpineBase},
            { JointType.SpineMid, SpineMid},
            { JointType.Neck, Neck},
            { JointType.Head, Head},
            { JointType.ShoulderRight, ShoulderLeft},
            { JointType.ElbowRight, ElbowLeft},
            { JointType.WristRight, WristLeft},
            { JointType.HandRight, HandLeft},
            { JointType.ShoulderLeft, ShoulderRight},
            { JointType.ElbowLeft, ElbowRight},
            { JointType.WristLeft, WristRight},
            { JointType.HandLeft, HandRight},
            { JointType.HipRight, HipLeft},
            { JointType.KneeRight, KneeLeft},
            { JointType.AnkleRight, AnkleLeft},
            { JointType.FootRight, FootLeft},
            { JointType.HipLeft, HipRight},
            { JointType.KneeLeft, KneeRight},
            { JointType.AnkleLeft, AnkleRight},
            { JointType.FootLeft, FootRight},
            { JointType.SpineShoulder, SpineShoulder},
            { JointType.HandTipRight, HandTipLeft},
            { JointType.ThumbRight, ThumbLeft},
            { JointType.HandTipLeft, HandTipRight},
            { JointType.ThumbLeft, ThumbRight}
        };
    }
    public Vector3 offset = Vector3.zero;
    public float lerp = 30f;

    public void Update()
    {
        Quaternion q = Quaternion.identity;
        Body body = GetTrackedBody();
        int i = 0;
        if (body != null)
        {
            if (countDown != null && !countDown.gameObject.activeSelf)
                countDown.gameObject.SetActive(true);

            i = 0;
            for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, i++)
            {
                if (_BoneMap[jt] != null && body.Joints[jt].TrackingState == TrackingState.Tracked)
                {
                    Quaternion rotation = Vector4ToQuaternion(body.JointOrientations[jt].Orientation);
                    rotation = rotation * Quaternion.Euler(additiveRotations[i]);
                    if (Quaternion.Angle(_BoneMap[jt].transform.rotation, rotation) > 3f)
                        _BoneMap[jt].transform.rotation = Quaternion.Lerp(_BoneMap[jt].transform.rotation, rotation, Time.deltaTime * lerp);

                }
            }
            Vector3 leftLegPosition = CameraSpacePointToVector3(body.Joints[JointType.FootLeft].Position);
            Vector3 rightLegPosition = CameraSpacePointToVector3(body.Joints[JointType.FootRight].Position);

            float avgScale = 1.5f;
            float currentAngle = (float)bodySourceManager.tiltRadians;
            float cos = Mathf.Cos(currentAngle);
            float sin = Mathf.Sin(currentAngle);
            
            Vector3 spinePosition = CameraSpacePointToVector3(body.Joints[JointType.SpineBase].Position);
            float newY = spinePosition.y * cos + spinePosition.z * sin;
            float newZ = spinePosition.z * cos - spinePosition.y * sin;
            spinePosition.y = newY;
            spinePosition.z = newZ;

            spinePosition = spinePosition * avgScale + offset;
            spinePosition.x = Mathf.Clamp(spinePosition.x, -1f, 1f);
            spinePosition.z = Mathf.Clamp(spinePosition.z, -1f, 1f);
            _BoneMap[JointType.SpineBase].transform.position = spinePosition;
        }
        else
        {
            _BoneMap[JointType.SpineBase].transform.position = Vector3.forward* 50f;
        }

       /* i = 0;
        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++, i++)
        if (_BoneMap[jt] != null){
            _BoneMap[jt].transform.rotation = Quaternion.Slerp(_BoneMap[jt].transform.rotation, lastQuaternions[i] , Time.deltaTime * lerp);
        }*/
    }

    public Body GetTrackedBody()
    {
        Body[] bodies = bodySourceManager.GetData();
        if (bodies == null)
            return null;
        foreach (Body b in bodySourceManager.GetData())
            if (b.IsTracked)
                return b;

        return null;
    }

    Vector3 GetScale(Vector3 v1, Vector3 v2)
    {
        Vector3 result = Vector3.zero;
        result.x = v1.x/v2.x;
        result.y = v1.y/v2.y;
        result.z = v1.z/v2.z;
        return result;
    }

    public Quaternion Vector4ToQuaternion(Windows.Kinect.Vector4 kinectVector4)
    {
        return new Quaternion(kinectVector4.X, kinectVector4.Y, kinectVector4.Z, kinectVector4.W);
    }

    public Vector3 CameraSpacePointToVector3(CameraSpacePoint csp)
    {
        return new Vector3(csp.X, csp.Y, csp.Z);
    }
}
