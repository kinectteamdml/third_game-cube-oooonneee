﻿using UnityEngine;
using System.Collections;

public class DialogQQ : MonoBehaviour
{

    public GameObject dialog;

    public void Quit()
    {
        Application.Quit();
    }
    public void DontQuit()
    {
        dialog.SetActive(false);
        Time.timeScale = 1f;
    }


    // Use this for initialization
    void Start()
    {
        Screen.SetResolution(Display.displays[0].systemWidth, Display.displays[0].systemHeight, true);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (dialog.activeSelf)
            {
                dialog.SetActive(false);
                Time.timeScale = 1f;
            }
            else
            {
                dialog.SetActive(true);
                Time.timeScale = 0f;
            }
        }
    }
}
