﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;


public class SecondTest : MonoBehaviour {



    public BodySourceManager _BodyManager;

    // Use this for initialization
    void Start () {
        Debug.Log(Kinect.JointType.WristRight.ToString());

       
        /*






SpineBase	0  	Base of the spine
SpineMid	1  	Middle of the spine
Neck	2  	Neck
Head	3  	Head

ShoulderLeft	4  	Left shoulder
ShoulderRight	8  	Right shoulder
ElbowLeft	5  	Left elbow
ElbowRight	9  	Right elbow
WristLeft	6  	Left wrist
WristRight	10  	Right wrist
HandLeft	7  	Left hand
HandRight	11  	Right hand

HipLeft	12  	Left hip
HipRight	16  	Right hip
KneeLeft	13  	Left knee
KneeRight	17  	Right knee
AnkleLeft	14  	Left ankle
AnkleRight	18  	Right ankle
FootLeft	15  	Left foot
FootRight	19  	Right foot

SpineShoulder	20  	Spine at the shoulder

HandTipLeft	21  	Tip of the left hand
HandTipRight	23  	Tip of the right hand

ThumbLeft	22  	Left thumb
ThumbRight	24  	Right thumb



        */

        /*  GameObject hips = GameObject.Find("hips");
          hipStart = hips.transform.localRotation;
          GameObject spine = GameObject.Find("spine");
          spineStart = spine.transform.localRotation;
          GameObject neck = GameObject.Find("neck");
          neckStart = neck.transform.localRotation;
          GameObject head = GameObject.Find("head");
          headStart = head.transform.localRotation;
          GameObject chest = GameObject.Find("chest");
          chestStart = chest.transform.localRotation;

          GameObject leftClavicle = GameObject.Find("clavicle.L");
          leftClavicleStart = leftClavicle.transform.localRotation;
          GameObject leftArm = GameObject.Find("upper_arm.L");
          leftArmStart = leftArm.transform.localRotation;
          GameObject leftForearm = GameObject.Find("forearm.L");
          leftForearmStart = leftForearm.transform.localRotation;
          GameObject leftHand = GameObject.Find("hand.L");
          leftHandStart = leftHand.transform.localRotation;

          GameObject rightClavicle = GameObject.Find("clavicle.R");
          rightClavicleStart = rightClavicle.transform.localRotation;
          GameObject rightArm = GameObject.Find("upper_arm.R");
          rightArmStart = rightArm.transform.localRotation;
          GameObject rightForearm = GameObject.Find("forearm.R");
          rightForearmStart = rightForearm.transform.localRotation;
          GameObject rightHand = GameObject.Find("hand.R");
          rightHandStart = rightHand.transform.localRotation;*/

    }

    public Quaternion getQ(Kinect.Vector4 v4)
    {
        return new Quaternion(v4.X, v4.Y, v4.Z, v4.W);
    }
	
	// Update is called once per frame
	void Update () {

        int q = 1;
        Kinect.Body[] data = _BodyManager.GetData();
        if (data != null  )
        {
            foreach (var item in data)
            {
                Quaternion quat  = getQ(item.JointOrientations[Kinect.JointType.WristLeft].Orientation);
               
                    Debug.Log(quat + "  " + q.ToString());
                
                q++;


            }
        }
        /*
        // get positions from kinect
        Vector3 neckPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.Neck].Position);
        Vector3 headPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.Head].Position);
        Vector3 leftHipPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.HipLeft].Position);
        Vector3 spineBasePosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.SpineBase].Position);
        Vector3 spineMidPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.SpineMid].Position);
        Vector3 spineShoulderPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.SpineShoulder].Position);
        Vector3 leftShoulderPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.ShoulderLeft].Position);
        Vector3 leftElbowPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.ElbowLeft].Position);
        Vector3 leftHandPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.HandLeft].Position);
        Vector3 leftWristPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.WristLeft].Position);
        Vector3 leftHandTipPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.HandTipLeft].Position);
        Vector3 leftThumbPosition = GetVector3FromCameraSpacePoint(body.Joints[Kinect.JointType.ThumbLeft].Position);

        // get rotations from kinect
        Quaternion neckOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.Neck].Orientation);
        Quaternion headOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.Head].Orientation);
        Quaternion leftHipOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.HipLeft].Orientation);
        Quaternion spineBaseOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.SpineBase].Orientation);
        Quaternion spineMidOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.SpineMid].Orientation);
        Quaternion spineShoulderOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.SpineShoulder].Orientation);
        Quaternion leftShoulderOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.ShoulderLeft].Orientation);
        Quaternion leftElbowOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.ElbowLeft].Orientation);
        Quaternion leftHandOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.HandLeft].Orientation);
        Quaternion leftWristOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.WristLeft].Orientation);
        Quaternion leftHandTipOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.HandTipLeft].Orientation);
        Quaternion leftThumbOrientation = QuaternionFromVector4(body.JointOrientations[Kinect.JointType.ThumbLeft].Orientation);
        */



    }
}
