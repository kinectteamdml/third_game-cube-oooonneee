﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

public class KinectOneSkin : MonoBehaviour
{

    public GameObject countDown;

    public GameObject root;
    public float deltaY = -0.5f;
    public float deltaX = 1;
    public float xPosition = 1.7f;
    public float deltaMove = 0.5f;


    public float moveKoef = 5;
    public float minRotateAngle = 0.1f;

    public GameObject FootLeft;
    public GameObject AnkleLeft;
    public GameObject KneeLeft;
    public GameObject HipLeft;

    public GameObject FootRight;
    public GameObject AnkleRight;
    public GameObject KneeRight;
    public GameObject HipRight;

    public GameObject HandTipLeft;
    public GameObject ThumbLeft;
    public GameObject HandLeft;
    public GameObject WristLeft;
    public GameObject ElbowLeft;
    public GameObject ShoulderLeft;

    public GameObject HandTipRight;
    public GameObject ThumbRight;
    public GameObject HandRight;
    public GameObject WristRight;
    public GameObject ElbowRight;
    public GameObject ShoulderRight;

    public GameObject SpineBase;
    public GameObject SpineMid;
    public GameObject SpineShoulder;
    public GameObject Neck;



    Vector3 v1;
    Vector3 v2;

    private Quaternion[] lastQ;

    public float lerpValue = 0.2f;

    public int player = 0;

    public Material BoneMaterial;
    public GameObject BodySourceManager;

    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;

    private List<Kinect.JointType> moveJoints;

    Vector3 firstBasePos;

    private Dictionary<Kinect.JointType, GameObject> _BoneMap = new Dictionary<Kinect.JointType, GameObject>()
    {
        /*{ Kinect.JointType.FootLeft, null },
        { Kinect.JointType.AnkleLeft, null },
        { Kinect.JointType.KneeLeft, null },
        { Kinect.JointType.HipLeft, null },

        { Kinect.JointType.FootRight, null },
        { Kinect.JointType.AnkleRight, null },
        { Kinect.JointType.KneeRight, null },
        { Kinect.JointType.HipRight, null },

        { Kinect.JointType.HandTipLeft, null },
        { Kinect.JointType.ThumbLeft, null },
        { Kinect.JointType.HandLeft, null },
        { Kinect.JointType.WristLeft, null },
        { Kinect.JointType.ElbowLeft, null},
        { Kinect.JointType.ShoulderLeft, null },

        { Kinect.JointType.HandTipRight, null },
        { Kinect.JointType.ThumbRight, null },
        { Kinect.JointType.HandRight, null },
        { Kinect.JointType.WristRight, null },
        { Kinect.JointType.ElbowRight, null },
        { Kinect.JointType.ShoulderRight, null },

        { Kinect.JointType.SpineBase, null},
        { Kinect.JointType.SpineMid, null },
        { Kinect.JointType.SpineShoulder, null },
        { Kinect.JointType.Neck,null },*/
    };

    List<Kinect.JointType> jTypes
     = new List<Kinect.JointType>()
        {

         { Kinect.JointType.FootLeft },
        { Kinect.JointType.AnkleLeft},
        { Kinect.JointType.KneeLeft },
        { Kinect.JointType.HipLeft },

        { Kinect.JointType.FootRight },
        { Kinect.JointType.AnkleRight },
        { Kinect.JointType.KneeRight },
        { Kinect.JointType.HipRight },

        { Kinect.JointType.HandTipLeft },
        { Kinect.JointType.ThumbLeft },
        { Kinect.JointType.HandLeft },
        { Kinect.JointType.WristLeft },
        { Kinect.JointType.ElbowLeft},
        { Kinect.JointType.ShoulderLeft },

        { Kinect.JointType.HandTipRight },
        { Kinect.JointType.ThumbRight },
        { Kinect.JointType.HandRight },
        { Kinect.JointType.WristRight },
        { Kinect.JointType.ElbowRight },
        { Kinect.JointType.ShoulderRight },

        { Kinect.JointType.SpineBase},
        { Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineShoulder },
        { Kinect.JointType.Neck },
};



    private Dictionary<Kinect.JointType, Kinect.JointType> _JoinMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };





    private Dictionary<Kinect.JointType, Kinect.JointType> MyMap;

    void Start()
    {

        firstBasePos = SpineBase.transform.position;


        lastQ = new Quaternion[jTypes.Count];

        _BoneMap = new Dictionary<Kinect.JointType, GameObject>()
    {
        { Kinect.JointType.FootLeft, FootLeft },
        { Kinect.JointType.AnkleLeft, AnkleLeft },
        { Kinect.JointType.KneeLeft, KneeLeft },
        { Kinect.JointType.HipLeft, HipLeft },

        { Kinect.JointType.FootRight, FootRight },
        { Kinect.JointType.AnkleRight, AnkleRight },
        { Kinect.JointType.KneeRight, KneeRight },
        { Kinect.JointType.HipRight, HipRight },

        { Kinect.JointType.HandTipLeft, HandTipLeft },
        { Kinect.JointType.ThumbLeft, ThumbLeft },
        { Kinect.JointType.HandLeft, HandLeft },
        { Kinect.JointType.WristLeft, WristLeft },
        { Kinect.JointType.ElbowLeft, ElbowLeft},
        { Kinect.JointType.ShoulderLeft, ShoulderLeft },

        { Kinect.JointType.HandTipRight, HandTipRight },
        { Kinect.JointType.ThumbRight, ThumbRight },
        { Kinect.JointType.HandRight, HandRight },
        { Kinect.JointType.WristRight, WristRight },
        { Kinect.JointType.ElbowRight, ElbowRight },
        { Kinect.JointType.ShoulderRight, ShoulderRight },

        { Kinect.JointType.SpineBase, SpineBase},
        { Kinect.JointType.SpineMid, SpineMid },
        { Kinect.JointType.SpineShoulder, SpineShoulder },
        { Kinect.JointType.Neck,Neck },
    };



    }

    public void Rotate(Kinect.Body[] data)
    {
        foreach (var item in data)
        {
           
            if (item.IsTracked)
            {
                if (countDown != null && !countDown.gameObject.activeSelf)
                {
                    countDown.gameObject.SetActive(true);
                }

                int i = 0;
                foreach (var _type in jTypes)
                {
                    GameObject bone = _BoneMap[jTypes[i]];
                    if (bone != null)
                    {

                        Quaternion _new = getQ(item.JointOrientations[_type].Orientation);



                        if (i >= 0 && i <= 3)
                        {
                            _new = _new * Quaternion.Euler(0, 90 , 0);

                        }
                        if (i >= 4 && i <= 7)
                        {
                            _new = _new * Quaternion.Euler(0, -90 , 0);

                        }
                        if ((i >= 8 && i <= 25))
                        {
                            _new = _new * Quaternion.Euler(0, 180, 0);
                        }
                        if (i >= 11 && i <= 12)
                        {

                        }

                        float lerpKoef = 1;


                        //for legs
                        if (i >= 0 && i <= 7)
                        {

                            if ((i + 1) % 4 != 0)
                            {
                                lerpKoef = (i + 1) % 4 * 0.1f * 2 + 0.4f;
                            }

                        }
                        //for arms
                        if (i >= 8 && i <= 16)
                        {

                            if ((i + 1) % 5 != 0)
                            {
                                lerpKoef = (i + 1) % 5 * 0.1f * 3;
                            }

                        }

                        //for hand only
                        if (i == 10 || i == 16)
                        {
                            lerpKoef = 0.1f;
                        }



                        //Debug.Log(i + " : " + _type + lerpKoef);
                        Quaternion lastRoot = bone.transform.rotation;
                        Quaternion newRoot = Quaternion.RotateTowards(bone.transform.rotation, _new, lerpValue * lerpKoef * Time.deltaTime * 63);
                        if (Quaternion.Angle(lastRoot,newRoot)> minRotateAngle)
                        {
                            bone.transform.rotation = Quaternion.Lerp(bone.transform.rotation, newRoot,0.5f * Time.deltaTime * 63) ;
                        }

                    }
                    i++;
                }


            }
        }
    }
    void Update()
    {
        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            root.transform.position = Vector3.up * -222;
            return;
        }

        Rotate(data);

        // move base join
        bool _end = true;
        foreach (var item in data)
        {
            if (item.IsTracked)
            {
                //Vector3 v3 = getV(item.Joints[Kinect.JointType.SpineBase].Position);
                Vector3 v3 = (getV(item.Joints[Kinect.JointType.AnkleLeft].Position) + getV(item.Joints[Kinect.JointType.AnkleRight].Position)) / 2;
                float all = Vector3.Distance(  getV(item.Joints[Kinect.JointType.AnkleLeft].Position) , getV(item.Joints[Kinect.JointType.KneeLeft].Position)  ) + Vector3.Distance(getV(item.Joints[Kinect.JointType.KneeLeft].Position) , getV(item.Joints[Kinect.JointType.HipLeft].Position));

                float h1 = getV(item.Joints[Kinect.JointType.HipLeft].Position).y - getV(item.Joints[Kinect.JointType.AnkleLeft].Position).y;
                float h2 = getV(item.Joints[Kinect.JointType.HipRight].Position).y - getV(item.Joints[Kinect.JointType.AnkleRight].Position).y;

                float hight = getV(item.Joints[Kinect.JointType.AnkleLeft].Position).y > getV(item.Joints[Kinect.JointType.AnkleRight].Position).y ? all - h2 : all - h1;


                float valueX = Mathf.Clamp(v3.x, -deltaMove, deltaMove);
                float valueZ = Mathf.Clamp(v3.z, -deltaMove+ xPosition, deltaMove+ xPosition);
                //float valueZ = v3.z;
                Debug.Log(valueZ);

                root.transform.position = new Vector3(valueX, v3.y, valueZ) * moveKoef + firstBasePos - Vector3.up * deltaY + Vector3.forward * deltaX// - Vector3.up * hight* moveKoef/2
                    ;
                //Debug.Log(v3);
                _end = false;
            }
            
        }
        if (_end)
        {
            root.transform.position = Vector3.up * -222;
        }
    }

    public Quaternion getQ(Kinect.Vector4 v4)
    {
        return new Quaternion(v4.X, v4.Y, v4.Z, v4.W);
    }

    public Vector3 getV(Kinect.CameraSpacePoint point)
    {
        return new Vector3(point.X, point.Y, point.Z);
    }

}
