﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
using System;

public class BodySourceManager : MonoBehaviour
{
    private KinectSensor _Sensor;
    private BodyFrameReader _Reader;
    private Body[] _Data = null;
    public double tiltRadians, tiltDegrees;
    public Body[] GetData()
    {
        return _Data;
    }


    void Start()
    {
        _Sensor = KinectSensor.GetDefault();

        if (_Sensor != null)
        {
            _Reader = _Sensor.BodyFrameSource.OpenReader();

            if (!_Sensor.IsOpen)
            {
                _Sensor.Open();
            }
        }
    }

    void Update()
    {
        if (_Reader != null)
        {
            var frame = _Reader.AcquireLatestFrame();
            if (frame != null)
            {
                if (_Data == null)
                {
                    _Data = new Body[_Sensor.BodyFrameSource.BodyCount];
                }
                CalculateTilt(frame);
                frame.GetAndRefreshBodyData(_Data);

                frame.Dispose();
                frame = null;
            }
        }
    }

    void CalculateTilt(BodyFrame f)
    {
        try
        {
            Windows.Kinect.Vector4 floorClipPlane;
            floorClipPlane = f.FloorClipPlane;
            tiltRadians = Mathf.Atan(floorClipPlane.Z / floorClipPlane.Y);
            tiltDegrees = tiltRadians * 180 / Mathf.PI;
        }
        catch (Exception e)
        {
        }
    }

    void OnApplicationQuit()
    {
        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }

        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }

            _Sensor = null;
        }
    }
}
