﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plate : MonoBehaviour
{
    public GameObject collider;
    public Transform Centr;

    public string myTag = "hand";

    public Material diagOn, diagOff;
    public Material standartOn, standartOff;
    public Material squaerOn, squaerOff;

    public Material active;

    GameObject[] gos;

    public List<GameObject> onObjects;

    public List<Collider> usedColliders;

    public int activeObjectsNumber = 0; 

    public float lenghtOfOnObject = 1;

    float timer = 1;

    public bool PlateOn = false;

    // Use this for initialization
    void Start()
    {
        onObjects = new List<GameObject>();
        usedColliders = new List<Collider>();

        gos = new GameObject[9];
        for (int i = 0; i < gos.Length; i++)
        {
            gos[i] = transform.GetChild(i).gameObject;
        }


        for (int i = 0; i < gos.Length; i++)
        {
            if (i + 1 == 5)
            {
                gos[i].GetComponent<MeshRenderer>().material = squaerOff;
            }
            else
            {
                if ((i + 1) % 2 == 1)
                {
                    gos[i].GetComponent<MeshRenderer>().material = diagOff;
                }
                else
                {
                    gos[i].GetComponent<MeshRenderer>().material = standartOff;
                }
            }

        }



    }

    public bool Hard_1()
    {
        activeObjectsNumber = 0;

        int i = Random.Range((int)0, (int)8);
        int last = i;

        int chose = 0;

        int k = 0;
        int j = 0;

        k = Random.Range((int)0, (int)7);
        int koef = k;

        switch (k)
        {
            case 0:
                if (i % 3 != 2)
                {
                    i += 1;
                }
                else
                {
                    i = -1;
                }

                break;
            case 1:
                if (i % 3 != 0)
                {
                    i -= 1;
                }
                else
                {
                    i = -1;
                }

                break;
            case 2:
                i -= 3;
                break;
            case 3:
                i += 3;
                break;
            case 4:
                if (i % 3 != 2)
                {
                    k = i / 3;
                    j = i % 3;
                    k++;
                    j++;
                    i = k * 3 + j;
                }
                else
                {
                    i = -1;
                }
                break;
            case 5:
                if (i % 3 != 2)
                {
                    k = i / 3;
                    j = i % 3;
                    k--;
                    j++;
                    i = k * 3 + j;
                }
                else
                {
                    i = -1;
                }
                break;
            case 6:
                if (i % 3 != 0)
                {
                    k = i / 3;
                    j = i % 3;
                    k++;
                    j--;
                    i = k * 3 + j;
                }
                else
                {
                    i = -1;
                }
                break;
            case 7:
                if (i % 3 != 0)
                {
                    k = i / 3;
                    j = i % 3;
                    k--;
                    j--;
                    i = k * 3 + j;
                }
                else
                {
                    i = -1;
                }
                break;
        }


        if (i < 0 || i > 8 || last < 0 || last > 8)
        {
            return false;
        }
        //Debug.Log(last + " \\ " + i + "(" + koef);
        Off();
        Chose(i);
        Chose(last);

        return true;


    }

    public bool Hard_2()
    {
        activeObjectsNumber = 0;

        int i = Random.Range((int)0, (int)8);

        int k = Random.Range((int)0, (int)4);
        int last = i;
        int koef = k;

        switch (k)
        {
            case 0:
                if (i % 3 == 0)
                {
                    i += 2;
                }
                else
                {
                    i = -1;
                }
                break;
            case 1:
                if (i % 3 == 2)
                {
                    i -= 2;
                }
                else
                {
                    i = -1;
                }
                break;
            case 2:
                if (i / 3 == 0)
                {
                    i += 6;
                }
                else
                {
                    i = -1;
                }
                break;
            case 3:
                if (i / 3 == 2)
                {
                    i -= 6;
                }
                else
                {
                    i = -1;
                }
                break;
            case 4:
                {
                    switch (i)
                    {
                        case 0:
                            i = 9;
                            break;
                        case 9:
                            i = 0;
                            break;
                        case 2:
                            i = 6;
                            break;
                        case 6:
                            i = 2;
                            break;
                        default:
                            i = -1;
                            break;
                    }
                }
                break;


        }

        if (i < 0 || i > 8 || last < 0 || last > 8)
        {
            return false;
        }
        //Debug.Log(last + " \\ " + i + "(" + koef);
        Off();
        Chose(i);
        Chose(last);

        return true;

    }


    public void Chose(int i)
    {
        if (i + 1 == 5)
        {
            gos[i].GetComponent<MeshRenderer>().material = squaerOn;
        }
        else
        {
            if ((i + 1) % 2 == 1)
            {
                gos[i].GetComponent<MeshRenderer>().material = diagOn;
            }
            else
            {
                gos[i].GetComponent<MeshRenderer>().material = standartOn;
            }
        }

        GameObject go = Instantiate(collider);
        go.transform.position = gos[i].transform.position;
        onObjects.Add(go);
        
        Trigger trig = go.AddComponent<Trigger>();
        trig.myPlate = gameObject.GetComponent<Plate>();
        trig.index = i;
        Vector3 dir = Centr.position - gos[i].transform.position;

        float x = dir.x;
        float y = dir.y;
        float z = dir.z;

        if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (Mathf.Abs(x) > Mathf.Abs(z))
            {
                dir = Vector3.right * x;
            }
            else
            {
                dir = Vector3.forward * z;
            }
        }
        else
        {
            if (Mathf.Abs(y) > Mathf.Abs(z))
            {
                dir = Vector3.up * y;
            }
            else
            {
                dir = Vector3.forward * z;
            }
        }

        dir = dir.normalized;
        go.transform.position += dir * lenghtOfOnObject;




    }



    public void Off()
    {
        usedColliders.Clear();

        for (int i = 0; i < gos.Length; i++)
        {
            if (i + 1 == 5)
            {
                gos[i].GetComponent<MeshRenderer>().material = squaerOff;
            }
            else
            {
                if ((i + 1) % 2 == 1)
                {
                    gos[i].GetComponent<MeshRenderer>().material = diagOff;
                }
                else
                {
                    gos[i].GetComponent<MeshRenderer>().material = standartOff;
                }
            }

        }

        GameObject[] buf = onObjects.ToArray();

        foreach (var item in buf)
        {
            GameObject go = item;
            Destroy(go);
        }

        onObjects.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        /*timer -= Time.deltaTime;

        if (timer < 0)
        {
            while (!Hard_1()) ;
            //Hard_1();
            timer = 10f;
        }*/

    }
}
