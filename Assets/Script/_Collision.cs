﻿using UnityEngine;
using System.Collections;

public class _Collision : MonoBehaviour {


    public GameObject go;

    public Transform target;

    public float lerpValue;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Debug.Log(go.transform.position);
        go.transform.position = Vector3.Lerp(go.transform.position,target.transform.position,Time.deltaTime*lerpValue);
        Debug.Log(go.transform.position);

        //Vector3 dir = (target.transform.position - go.transform.position).normalized;
        //go.transform.position += dir * Time.deltaTime * lerpValue;
    }
}
