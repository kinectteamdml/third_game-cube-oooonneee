﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowLevel : MonoBehaviour {

    Text t;
    public float time = 1.5f;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        t.enabled = false;
	}
	
    public void Show_Level()
    {
        int value = PlatesController.Instance.hard;
        StartCoroutine(Show(time,value));
    }

    IEnumerator Show(float time,int value)
    {
        t.enabled = true;
        t.text = "Level " + value.ToString();
        yield return new WaitForSeconds(time);
        t.enabled = false;
    }
    
}
