﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
public class PlatesController : MonoBehaviour {


    public WellDone wDone;

    private static PlatesController instance_;
    public static PlatesController Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<PlatesController>();
            return instance_;
        }
    }

    public GameParams gameParams = new GameParams();
    public Text RoundTime;

    public MusicSound mSound;
    public Plate[] plates;
    public Plate downPlate;
    private int numberOfPlate = 0;

    public UnityEvent NewLevel;

    public Text score;


    private Task task;
    private List<Wall> walls = new List<Wall>();

    Plate oneOfThree;
    public float roundTime = 20;
    float _timer;

    public int hard = 1;
    public float colorTime = 1;
    float timer = 0;

    bool allOn = false;

    bool wait = false;

    public void SetPlates()
    {
        //int i = UnityEngine.Random.Range((int)0, (int)3);
        if (numberOfPlate == 3)
        {
            numberOfPlate = 0;
            if (OutFileController.Instance.SuccsesLastSession(task))
            {
                hard++;
                hard = Mathf.Clamp(hard, (int)1, (int)2);
                NewLevel.Invoke();
            }

        }

        int i = numberOfPlate;
        oneOfThree = plates[i];
        numberOfPlate++;
       
        

        switch (hard)
        {
            case 1:
                {
                    while (!downPlate.Hard_1()) ;
                    while (!oneOfThree.Hard_1()) ;

                }

                break;
            case 2:
                {
                    while (!downPlate.Hard_2()) ;
                    while (!oneOfThree.Hard_2()) ;
                    //downPlate.Hard_2();
                    //oneOfThree.Hard_2();
                }
                break;
            default:
                break;
        }

        Wall wall = new Wall(downPlate.name,downPlate.onObjects[0].GetComponent<Trigger>().index, downPlate.onObjects[1].GetComponent<Trigger>().index);
        walls.Add(wall);
        wall = new Wall(oneOfThree.name, oneOfThree.onObjects[0].GetComponent<Trigger>().index, oneOfThree.onObjects[1].GetComponent<Trigger>().index);
        walls.Add(wall);

        _timer = roundTime;
    }

    IEnumerator WaitStart()
    {
        yield return new WaitForSeconds(0.1f);
        SetPlates();
        
        task = new Task(DateTime.Now.ToString(), "", walls);
        OutFileController.Instance.NumberOfTasks++;
       
   
      
    }

    IEnumerator StartFinalColor()
    {
        wait = true;
        mSound.MyPlay();
          
        foreach (var item in oneOfThree.onObjects)
        {

            //item.gameObject.GetComponent<MeshRenderer>().material = oneOfThree.active;

            oneOfThree.transform.GetChild(item.gameObject.GetComponent<Trigger>().index).GetComponent<MeshRenderer>().material= oneOfThree.active;
        }
        foreach (var item in downPlate.onObjects)
        {

            item.gameObject.GetComponent<MeshRenderer>().material = downPlate.active;

            downPlate.transform.GetChild(item.gameObject.GetComponent<Trigger>().index).GetComponent<MeshRenderer>().material = downPlate.active;
        }

        //WellDone.Instance.ShowDone();
        wDone.ShowDone();

        yield return new WaitForSeconds(0.7f);
         
        downPlate.Off();
        oneOfThree.Off();

        yield return new WaitForSeconds(0.7f); 

        task.endTaskTime = DateTime.Now.ToString();

        //////
        OutFileController.Instance.addTask(task);
        OutFileController.Instance.NumberOfSuccessTasks++;
        OutFileController.Instance.SuccessTask();
        score.text = (int.Parse(score.text) + 5).ToString();
        walls = new List<Wall>();


        SetPlates();
        task = new Task(DateTime.Now.ToString(), "", walls);

        allOn = false;
        wait = false;

    }
    IEnumerator Lost()
    {
        wait = true;
        downPlate.Off();
        oneOfThree.Off();

       // WellDone.Instance.ShowFail();
        wDone.ShowFail();

        yield return new WaitForSeconds(0.7f);
        wait = false;

        //task.endTaskTime = DateTime.Now.ToString();
        //////
        //OutFileController.Instance.addTask(task);
        
        walls = new List<Wall>();


        task.endTaskTime = DateTime.Now.ToString();
        task.success = false;

        //////
        OutFileController.Instance.addTask(task);
        OutFileController.Instance.NumberOfSuccessTasks++;
        walls = new List<Wall>();

        SetPlates();
        task = new Task(DateTime.Now.ToString(), "", walls);

        allOn = false;
        wait = false;

    }


    IEnumerator LoadParametres()
    {
        gameParams = gameParams.Load();
        yield return new WaitForSeconds(0.1f);
    }

    // Use this for initialization
    void Start () {
        //gameParams = gameParams.Load();
        StartCoroutine(LoadParametres());
        roundTime = gameParams.roundTime;



        StartCoroutine(WaitStart());
        _timer = roundTime;
        NewLevel.Invoke();
        score.text = "0";

        


	}
	
	// Update is called once per frame
	void Update () {
        if (!wait)
        {
            if (oneOfThree.activeObjectsNumber + downPlate.activeObjectsNumber == 4 && !allOn)
            {
                allOn = true;
                timer = colorTime;

            }

            if (oneOfThree.activeObjectsNumber + downPlate.activeObjectsNumber != 4)
            {
                allOn = false;
            }

            if (allOn)
            {
                timer -= Time.deltaTime;
                if (timer < 0)
                {
                    StartCoroutine(StartFinalColor());
                }
            }
            else
            {
                _timer -= Time.deltaTime;
                RoundTime.text = ((int)_timer).ToString();
            }


            if (Input.GetKeyDown(KeyCode.Space))
            {
                hard = hard == 1 ? 2 : 1;

            }

            
            if (_timer<0)
            {
               
                StartCoroutine(Lost());
            }
        }

	}
}
