﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelUpText : MonoBehaviour {
    public float alphaSpeed = 0.1f;
    public float resizeSpeed = 0.1f;
    Text text;
    public Color defaultColor;
    Color currColor;
    public void Start()
    {
        text = GetComponent<Text>();
        SetLevel(1);
        MusicController.Instance.LevelUp();
    }

    public void SetLevel(int k)
    {
        text.text = "level " + k;
        text.color = defaultColor;
        currColor = defaultColor;
        transform.localScale = Vector3.one;
    }

    void Update () {
        if (currColor.a > 0f)
        {
            transform.localScale += Vector3.one * resizeSpeed * Time.deltaTime;
            currColor.a -= alphaSpeed * Time.deltaTime;
            text.color = currColor;
        }
	}
}
