﻿using UnityEngine;
using System.Collections;

public class TriggerActivator : MonoBehaviour {

    public bool arm = true;


	// Use this for initialization
	void Start () {

        GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("someHit");

        if (other.tag.Equals("Enemy"))
        {
            Shape shape = other.gameObject.GetComponent<Shape>();
            if (arm)
            {
                shape.ArmHit(gameObject.name, other.transform.position);


                //Debug.Log("armHit");
            }
            else
            {
                shape.LegHit(gameObject.name, other.transform.position);


                //Debug.Log("legHit");
            }

        }
    }
}
