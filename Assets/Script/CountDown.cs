﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {
    public UnityEvent OnCountdownEnds;
    float timer = -1;
    public Text text;
    public void Start()
    {
        // text = GetComponent<Text>();
        GameParams gp = new GameParams();
        float value = gp.Load().gameTime;

        text.text = "";
        StartTimer(value);

    }

	void Update () {
        if (timer > 0)
        {
            
            timer -= Time.deltaTime;
            text.text = ((int)(timer)+1).ToString();
            //Debug.Log(timer);
            if (timer <= 0)
            {
                OnCountdownEnds.Invoke();
                text.text = "";
                Quit();
            }
        }
	}

    public void Quit()
    {
        Application.Quit();
    }

    public void StartTimer(float time)
    {
        timer = time;
    }
}
