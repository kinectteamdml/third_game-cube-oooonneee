﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour
{
    private static MusicController instance_;
    public static MusicController Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<MusicController>();
            return instance_;
        }
    }
    AudioSource music;
    AudioSource sounds;
    public AudioClip clip0;
    public AudioClip clip1;
    public AudioClip clip2;
    public AudioClip clip3;
    void Start()
    {
        music = GetComponents<AudioSource>()[0];
        sounds = GetComponents<AudioSource>()[1];
    }

    public void ArmHit()
    {
        sounds.clip = clip0;
        sounds.Play();
    }

    public void LegHit()
    {
        sounds.clip = clip1;
        sounds.Play();
    }

    public void BothHit()
    {
        sounds.clip = clip2;
        sounds.Play();
    }

    public void LevelUp()
    {
        sounds.clip = clip3;
        sounds.Play();
    }
}
