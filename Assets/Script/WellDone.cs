﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class WellDone : MonoBehaviour {
    public Color green;
    public Color red;

    public Text message;

    private static WellDone instance_;
    public static WellDone Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<WellDone>();
            return instance_;
        }
    }


    // Use this for initialization
    void Start () {
        message.enabled = false;
	}
	
    public void ShowDone()
    {
        message.color = green;
        StartCoroutine(Show("Well done!"));
    }

    public void ShowFail()
    {
        message.color = red;
        StartCoroutine(Show("Try again"));
    }



    IEnumerator Show(string text)
    {
        message.enabled = true;
        message.text = text;
        yield return new WaitForSeconds(2.5f);

        message.enabled = false;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
