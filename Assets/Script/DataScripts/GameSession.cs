﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System;
[Serializable]
[XmlRoot("Session")]
public class GameSession {
    [XmlAttribute("BuildVersion")]
    public string buildVersion = "1.054444";
    [XmlAttribute("PatientId")]
    public int patientId=0;
    [XmlAttribute("BeginningTime")]
    public string beginningTime;
    [XmlAttribute("EndingTime")]
    public string endingTime;
    [XmlAttribute("FinalScore")]
    public int finalScore=0;
    public int hittedCount = 0;
    [XmlAttribute("TotalNumber")]
    public int totalNumber=0;
    [XmlArray("Hits")]
    [XmlArrayItem("Hit")]
    public List<Hit> hits = new List<Hit>();
    [XmlArray("Changes")]
    [XmlArrayItem("Parameter")]
    public List<Parameter> changes = new List<Parameter>();

    public GameSession(){ 
    }

    public GameSession(int id)
    {
        this.patientId = id;
    }

    public void AddHit(string joint, Vector3 position, int type, string timeStamp)
    {
        Hit hit = new Hit(joint, position, type, timeStamp);
        hits.Add(hit);
    }

    public void AddHit(string joint, float x, float y, float z, int type, string timeStamp)
    {
        Hit hit = new Hit(joint, x, y, z, type, timeStamp);
        hits.Add(hit);
    }

    public void AddChange(string name, float oldValue, float newValue,string timeStamp)
    { 
        Parameter param = new Parameter(name, oldValue, newValue, timeStamp);
        changes.Add(param);
    }

    public void AddTotal(int delta)
    {
        totalNumber += delta;
    }

    public void AddHittedCount(int delta)
    {
        hittedCount += delta;
    }

    public void SetFinalScore(int score)
    {
        this.finalScore = score;
    }
    public void Save(string name)
    {
        String path = Application.dataPath + "/../out/" + name + ".xml";
        Debug.Log(path);
        XmlSerializer serializer = new XmlSerializer(typeof(GameSession));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public GameSession Load(string name)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(GameSession));
        try
        {
            Debug.Log(Application.persistentDataPath);
            using (var stream = new FileStream(Application.persistentDataPath + "/"+name+".xml", FileMode.Open))
            {
                return serializer.Deserialize(stream) as GameSession;
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            return new GameSession();
        }
    }

    public float GetPerformance()
    {
        if (this.totalNumber == 0) return 0;
        return (float)(this.hittedCount) /(float)(this.totalNumber);
    }
}
