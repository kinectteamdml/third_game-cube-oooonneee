﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System;
using System.Collections.Generic;
[Serializable]
public class Task{


    [XmlAttribute("StartTaskTime")]
    public string startTaskTime = "";
    [XmlAttribute("EndTaskTime")]
    public string endTaskTime = "";


    [XmlArray("Walls")]
    [XmlArrayItem("Wall")]
    public List<Wall> walls = new List<Wall>();

    [XmlAttribute("Success")]
    public bool success  = true;

    public Task()
    {

    }

    public Task(string start,string end,List<Wall> _walls)
    {
        startTaskTime = start;
        endTaskTime = end;
        walls = _walls;
    }

  
}
