﻿using UnityEngine;
using System.Collections;
using System;
[Serializable]

public class OutFileController : MonoBehaviour
{
    private static OutFileController instance_;
    public static OutFileController Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<OutFileController>();
            return instance_;
        }
    }

    public void addTask(Task _task)
    {
        gameSession.tasks.Add(_task);
    }

    public OutFile gameSession = new OutFile();
    public int NumberOfTasks;
    public int NumberOfSuccessTasks;

    // Use this for initialization
    void Start()
    {

        Debug.Log(DateTime.Now.ToString());

        int id = 0;
        if (Environment.GetCommandLineArgs().Length > 1)
        {
            String s = Environment.GetCommandLineArgs()[1].Remove(0, 1);
            int.TryParse(s, out id);
        }
        gameSession.patientID = id;
        gameSession.startTime = gameSession.endTime = DateTime.Now.ToString();
    }

    public void OnApplicationQuit()
    {
        gameSession.endTime = DateTime.Now.ToString();
        DateTime d = DateTime.Now;
        String s = d.Date.Month + "_" + d.Date.Day + "_" + d.Date.Year + "___" + d.Hour + "_" + d.Minute;
        gameSession.Save(s);
    }

    public void SuccessTask()
    {
        gameSession.finalScore += 5;
    }

    public bool SuccsesLastSession(Task task)
    {
        int index = gameSession.tasks.IndexOf(task);
        bool success = true;
        for (int i = index; i >= index-2; i--) // session include 3 elemenents
        {
            if (!gameSession.tasks[i].success)
            {
                return false;
            }
        }
        return true;
    }


    public void Update()
    {
        /*if (Input.GetKey(KeyCode.Escape))
            Application.Quit();*/
    }

}
