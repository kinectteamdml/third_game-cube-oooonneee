﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.IO;
[XmlRoot("Params")]
public class GameParams {
    [XmlAttribute("GameTime")]
    public float gameTime = 600f;
    [XmlAttribute("RoundTime")]
    public float roundTime = 20f;

    
    public GameParams()
    { 
    }

    public void Save()
    {
        String path = Application.dataPath + "/../params.xml";
        Debug.Log(path);
        XmlSerializer serializer = new XmlSerializer(typeof(GameParams));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public GameParams Load()
    {
        String path = Application.dataPath + "/../params.xml";
        XmlSerializer serializer = new XmlSerializer(typeof(GameParams));
        try
        {
            Debug.Log(Application.persistentDataPath);
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as GameParams;
            }
        }
        catch (Exception e)
        {
            GameParams gp = new GameParams();
            gp.Save();
            return gp;
        }
    }

}
