﻿using UnityEngine;
using System.Collections;
using System;
[Serializable]
public class GameSessionController : MonoBehaviour {
    private static GameSessionController instance_;
    public static GameSessionController Instance{
        get{
            if (instance_ == null) 
                instance_= GameObject.FindObjectOfType<GameSessionController>();
            return instance_;
        }
    }
    public GameSession gameSession = new GameSession();
    public void Start()
    {
        Debug.Log(DateTime.Now.ToString());
        
        int id =0;
        if (Environment.GetCommandLineArgs().Length > 1) { 
            String s = Environment.GetCommandLineArgs()[1].Remove(0,1);
            int.TryParse(s, out id);
        }
        gameSession.patientId = id;
        gameSession.beginningTime = DateTime.Now.ToString();
/*
        gameSession.AddScore(15);
        gameSession.AddTotal(20);
        gameSession.AddHit("elbow", 0f, 1f, 0.25f, 0, DateTime.Now.ToString());
        gameSession.AddHit("elbow", 0f, 2f, 0.25f, 1, DateTime.Now.ToString());
        gameSession.AddHit("elbow", 0f, 3f, 0.25f, 0, DateTime.Now.ToString());
        gameSession.AddHit("elbow", 0f, 4f, 0.25f, 1, DateTime.Now.ToString());
        gameSession.AddChange("spawnTime", 0f, 2f, DateTime.Now.ToString());
        gameSession.AddChange("size", 1f, 3f, DateTime.Now.ToString());
        gameSession.endingTime = DateTime.Now.ToString();
        gameSession.Save("test");
        End();*/
    }

    public void OnApplicationQuit()
    {
        gameSession.endingTime = DateTime.Now.ToString();
        DateTime d = DateTime.Now;
        String s = d.Date.Month +"_"+ d.Date.Day+"_"+d.Date.Year+"___"+ d.Hour+"_"+d.Minute;
        gameSession.Save(s);
    }

    public void Update()
    {
        /*if (Input.GetKey(KeyCode.Escape))
            Application.Quit();*/
    }
    public void AddHit(string joint, Vector3 position, int type, string timeStamp)
    {
        gameSession.AddHit(joint, position, type, timeStamp);
    }

    public void AddHit(string joint, Vector3 position, int type)
    {
        gameSession.AddHit(joint, position, type, DateTime.Now.ToString());
    }

    public void AddHit(string joint, float x, float y, float z, int type, string timeStamp)
    {
        gameSession.AddHit(joint, x, y, z, type, timeStamp);
    }

    public void AddHit(string joint, float x, float y, float z, int type)
    {
        gameSession.AddHit(joint, x, y, z, type, DateTime.Now.ToString());
    }

    public void AddChange(string name, float oldValue, float newValue, string timeStamp)
    {
        gameSession.AddChange(name, oldValue, newValue, timeStamp);
    }

    public void AddChange(string name, float oldValue, float newValue)
    {
        gameSession.AddChange(name, oldValue, newValue, DateTime.Now.ToString());
    }

    public void AddTotal(int delta)
    {
        gameSession.AddTotal(delta);
    }

    public void AddScore(int delta)
    {
        gameSession.AddHittedCount(delta);
    }

}
